import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GithubissueComponent } from './githubissue.component';

describe('GithubissueComponent', () => {
  let component: GithubissueComponent;
  let fixture: ComponentFixture<GithubissueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GithubissueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GithubissueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
