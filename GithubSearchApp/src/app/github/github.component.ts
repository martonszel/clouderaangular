import { Component, OnInit } from '@angular/core';
import { GithubService } from '../githubservice.service';
import { Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Repository } from '../models/repository';

@Component({
  selector: 'app-github',
  templateUrl: './github.component.html',
  styleUrls: ['./github.component.css']
})
export class GithubComponent implements OnInit {

  searchResult$: Observable<Repository>;

  constructor(private githubService: GithubService) { }

  onTextChange(query: string) {
    this.searchResult$ = this.githubService.searchRepo(query).pipe(debounceTime(500), distinctUntilChanged());
    // (this.githubService.searchRepo(query).subscribe(x=> console.log(x.items)))

    // localStorage.setItem('searchresult', JSON.stringify(this.searchResult$));

}


  ngOnInit() {
  }

}
