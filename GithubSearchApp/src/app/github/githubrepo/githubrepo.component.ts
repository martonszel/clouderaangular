import { Component, OnInit, Input, ViewChildren, QueryList } from '@angular/core';
import { Observable } from 'rxjs';
import { Repository } from 'src/app/models/repository';

@Component({
  selector: 'app-githubrepo',
  templateUrl: './githubrepo.component.html',
  styleUrls: ['./githubrepo.component.css']
})
export class GithubrepoComponent implements OnInit {


  @Input ()searchResult$ =[];

  page = 1;
  pageSize = 4;
  collectionSize = this.searchResult$.length;

  constructor() { }

  ngOnInit() {
  }

}
