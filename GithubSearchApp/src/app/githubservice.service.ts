import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { map, debounceTime, distinctUntilChanged,switchMap, tap, mergeMap, flatMap } from 'rxjs/operators';
import { Repository } from './models/repository';
import { Issue } from './models/issues';

const githubissueUrl = 'https://api.github.com/search/issues?q=repo:username/reponame';
const githubissueUrlhttps1 = 'https://api.github.com/repos/twbs/bootstrap/issues';
const githubissueUrl2 = 'issues_url": "https://api.github.com/repos/twbs/bootstrap/issues{/number}';

@Injectable({
  providedIn: 'root'
})
export class GithubService {

  id;
  name;
  issues;

constructor(private http: HttpClient) { }

searchRepo(query: string): Observable<any> {
  const params = { q: query };
  const githubrepoUrl = 'https://api.github.com/search/repositories';

  return this.http.get<Repository>(githubrepoUrl, { params }
).pipe(
  debounceTime(400),
  distinctUntilChanged(),
  tap(output => output.items.forEach(x => {
  //console.log(output.items)
  this.name = x.full_name,
  this.issues = this.http.get<Issue>(`https://api.github.com/repos/${this.name}/issues`).subscribe(b=> console.log(b));
  console.log(this.name); })),
  );

  
  
}




}

